var mysql = require('mysql');
var set = require('../settings/settings');

var con = mysql.createConnection({
  host: set.getHost(),
  user: set.getUser(),
  password: set.getPassword(),
  database: set.getDatabase()
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
});


//get all categories
exports.getSalary = function () {
  return new Promise(function (resolve, reject) {
    var query_str = "select * from salaries limit 50";
    con.query(query_str, function (err, rows, fields) {
      if (err) {
        return reject(err);
      }
      resolve(rows);
    });
  });
}
