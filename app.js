let express = require('express');
let app = express();
let bodyParser = require('body-parser')
let exphbs = require('express-handlebars');
const jwt = require('express-jwt');
const secret = {secret: process.env.SECRET || 'secret'}



app.set('view engine', 'handlebars');
app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.engine('handlebars', exphbs({
  defaultLayout: 'main'
}));


/********************************/
/********* M O D E L S **********/
/********************************/


let Employees = require('./data-models/emploeeModel');
let Department = require('./data-models/departmentModel');
let Salary = require('./data-models/salaryModel');
let Titles = require('./data-models/titlesModel');
let Manager = require('./data-models/deptManagerModel');



/********************************/
/**** E N D  M O D E L S ********/
/********************************/



/********************************/
/******* R O U T I N G **********/
/********************************/

//Get root path
app.get('/api/v1/employees',jwt (secret), function (req, res) {

	if(req.user.admin  === true ){
		Employees.getEmployees().then(function (rows) {
			return res.end(JSON.stringify(rows))
		});
	} else {
		res.status(401).send({message: "Uste no esta autorizado para ver esta ruta"})
	}

});

app.get('/api/v1/departments',jwt(secret), function (req, res) {

	Department.getDepartments().then(function (rows) {
		return res.end(JSON.stringify(rows))
	});
});

app.get('/api/v1/salaries',jwt(secret), function (req, res) {

	Salary.getSalary().then(function (rows) {
		return res.end(JSON.stringify(rows))
	});
});

app.get('/api/v1/titles',jwt(secret), function (req, res) {

	Titles.getTitles().then(function (rows) {
		return res.end(JSON.stringify(rows))
	});
});

app.get('/managers',jwt(secret), function (req, res) {

	Manager.getDeptManagers().then(function (rows) {
		return res.end(JSON.stringify(rows))
	});
});

/****************POST REQUEST********************************/
app.post('/api/v1/employees' ,jwt(secret), function (req, res) {

	let emp_no = req.body.emp_no;
	let birth_date = req.body.birth_date;
	let first_name = req.body.first_name;
	let last_name = req.body.last_name;
	let gender = req.body.gender;
	let hire_date = req.body.hire_date;

	const employee = {
		"emp_no": emp_no,
		"birth_date" : birth_date,
		"first_name": first_name,
		"last_name": last_name,
		"gender":gender,
		"hire_date":hire_date
	};

	Employees.addNewEmployee(employee).then(function (rows) {
		res.sendStatus(200)
	}).catch(error => console.log(error))


});


/********************************/
/***** E N D   R O U T E S ******/
/********************************/

app.listen(3000, function () {
  console.log('server running on port 3000!');
});
